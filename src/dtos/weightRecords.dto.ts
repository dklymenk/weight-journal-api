import { IsDateString, IsNotEmpty, IsNumber, IsOptional, IsPositive, IsString, Max } from 'class-validator';

export class CreateWeightRecordDto {
  @IsDateString()
  @IsNotEmpty()
  public date: string;

  @IsPositive()
  @IsNotEmpty()
  @IsNumber({ maxDecimalPlaces: 2 }, { message: 'weight must be a number with 2 decimal places' })
  @Max(9999)
  public weight: number;

  @IsOptional()
  @IsString()
  public note?: string;
}
