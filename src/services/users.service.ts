import { hash } from 'bcrypt';
import { User } from '@prisma/client';
import { CreateUserDto } from '@dtos/users.dto';
import { HttpException } from '@exceptions/HttpException';
import { isEmpty } from '@utils/util';
import { exclude, prisma } from '@utils/prisma';

class UserService {
  public users = prisma.user;

  public async findAllUser() {
    const allUser = await this.users.findMany({ select: exclude('user', ['password']) });
    return allUser;
  }

  public async findUserById(userId: number) {
    if (isEmpty(userId)) throw new HttpException(400, 'Invalid or missing user id');

    const findUser = await this.users.findUnique({ where: { id: userId }, select: exclude('user', ['password']) });
    if (!findUser) throw new HttpException(404, `User with id ${userId} cannot be found`);

    return findUser;
  }

  public async createUser(userData: CreateUserDto) {
    if (isEmpty(userData)) throw new HttpException(400, 'Invalid or missing user data');

    const findUser: User = await this.users.findUnique({ where: { email: userData.email } });
    if (findUser) throw new HttpException(409, `User with email ${userData.email} already exists`);

    const hashedPassword = await hash(userData.password, 10);
    const createUserData = await this.users.create({ data: { ...userData, password: hashedPassword }, select: exclude('user', ['password']) });
    return createUserData;
  }

  public async updateUser(userId: number, userData: CreateUserDto) {
    if (isEmpty(userData)) throw new HttpException(400, 'Invalid or missing user data');

    const findUser: User = await this.users.findUnique({ where: { id: userId } });
    if (!findUser) throw new HttpException(404, `User with id ${userId} cannot be found`);

    const hashedPassword = await hash(userData.password, 10);
    const updateUserData = await this.users.update({
      where: { id: userId },
      data: { ...userData, password: hashedPassword },
      select: exclude('user', ['password']),
    });
    return updateUserData;
  }

  public async deleteUser(userId: number) {
    if (isEmpty(userId)) throw new HttpException(400, 'Invalid or missing user id');

    const findUser: User = await this.users.findUnique({ where: { id: userId } });
    if (!findUser) throw new HttpException(404, `User with id ${userId} cannot be found`);

    const deleteUserData = await this.users.delete({ where: { id: userId }, select: exclude('user', ['password']) });
    return deleteUserData;
  }
}

export default UserService;
