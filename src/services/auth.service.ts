import { compare, hash } from 'bcrypt';
import { sign } from 'jsonwebtoken';
import { User } from '@prisma/client';
import { SECRET_KEY } from '@config';
import { CreateUserDto, LoginDto } from '@dtos/users.dto';
import { HttpException } from '@exceptions/HttpException';
import { DataStoredInToken, TokenData } from '@interfaces/auth.interface';
import { isEmpty } from '@utils/util';
import { exclude, prisma } from '@utils/prisma';

class AuthService {
  public users = prisma.user;

  public async signup(userData: CreateUserDto) {
    if (isEmpty(userData)) throw new HttpException(400, 'Invalid or missing user data');

    const findUser: User = await this.users.findUnique({ where: { email: userData.email } });
    if (findUser) throw new HttpException(409, `User with email ${userData.email} already exists`);

    const hashedPassword = await hash(userData.password, 10);

    const totalUsers = await this.users.count();
    const createUserData = this.users.create({
      data: { ...userData, password: hashedPassword, isAdmin: totalUsers === 0 },
      select: exclude('user', ['password']),
    });

    return createUserData;
  }

  public async login(userData: LoginDto): Promise<{ cookie: string; token: string }> {
    if (isEmpty(userData)) throw new HttpException(400, 'Invalid or missing user data');

    const findUser = await this.users.findUnique({ where: { email: userData.email }, select: { id: true, email: true, password: true } });
    if (!findUser) throw new HttpException(404, `User with email ${userData.email} cannot be found`);

    const isPasswordMatching: boolean = await compare(userData.password, findUser.password);
    if (!isPasswordMatching) throw new HttpException(409, 'Wrong password');

    const tokenData = this.createToken(findUser.id);
    const cookie = this.createCookie(tokenData);

    return { cookie, token: tokenData.token };
  }

  public async logout(userData: User): Promise<User> {
    if (isEmpty(userData)) throw new HttpException(400, 'Invalid or missing user data');

    const findUser: User = await this.users.findFirst({ where: { email: userData.email, password: userData.password } });
    if (!findUser) throw new HttpException(404, `User with email ${userData.email} cannot be found`);

    return findUser;
  }

  public createToken(id: User['id']): TokenData {
    const dataStoredInToken: DataStoredInToken = { id };
    const secretKey: string = SECRET_KEY;
    const expiresIn: number = 60 * 60;

    return { expiresIn, token: sign(dataStoredInToken, secretKey, { expiresIn }) };
  }

  public createCookie(tokenData: TokenData): string {
    return `Authorization=${tokenData.token}; HttpOnly; Max-Age=${tokenData.expiresIn};`;
  }
}

export default AuthService;
