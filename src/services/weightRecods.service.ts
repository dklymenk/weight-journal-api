import { CreateWeightRecordDto } from '@/dtos/weightRecords.dto';
import { HttpException } from '@/exceptions/HttpException';
import { prisma } from '@utils/prisma';

class WeightRecordsService {
  public weightRecords = prisma.weightRecord;

  public async findAllWeightRecords(userId: number, startDate?: string, endDate?: string) {
    const where = { userId };
    if (startDate && endDate) {
      where['date'] = { gte: new Date(startDate), lte: new Date(endDate) };
    } else if (startDate) {
      where['date'] = { gte: new Date(startDate) };
    } else if (endDate) {
      where['date'] = { lte: new Date(endDate) };
    }

    const data = await this.weightRecords.findMany({ where, orderBy: { date: 'desc' } });
    return data;
  }

  public async findWeightRecordById(weightRecordId: number, userId: number) {
    return await this.weightRecords.findFirst({ where: { id: weightRecordId, userId } });
  }

  public async createWeightRecord(weightRecordData: CreateWeightRecordDto, userId: number) {
    const createWeightRecordData = await this.weightRecords.create({ data: { ...weightRecordData, userId, date: new Date(weightRecordData.date) } });
    return createWeightRecordData;
  }

  public async updateWeightRecord(dto: CreateWeightRecordDto, weightRecordId: number, userId: number) {
    const record = await this.weightRecords.findFirst({ where: { id: weightRecordId, userId } });

    if (!record) throw new HttpException(404, `Weight record with id ${weightRecordId} cannot be found`);

    const data = await this.weightRecords.update({ where: { id: weightRecordId }, data: { ...dto, date: new Date(dto.date) } });
    return data;
  }

  public async deleteWeightRecord(weightRecordId: number, userId: number) {
    const record = await this.weightRecords.findFirst({ where: { id: weightRecordId, userId } });

    if (!record) throw new HttpException(404, `Weight record with id ${weightRecordId} cannot be found`);

    const data = await this.weightRecords.delete({ where: { id: weightRecordId } });
    return data;
  }
}

export default WeightRecordsService;
