/*
  Warnings:

  - You are about to alter the column `createdAt` on the `WeightRecord` table. The data in that column could be lost. The data in that column will be cast from `Timestamp(0)` to `Timestamp`.
  - Changed the type of `weight` on the `WeightRecord` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.

*/
DELETE FROM `WeightRecord`;
-- AlterTable
ALTER TABLE `WeightRecord` MODIFY `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    DROP COLUMN `weight`,
    ADD COLUMN `weight` DECIMAL(6, 2) NOT NULL;
