-- AlterTable
ALTER TABLE `User` ADD COLUMN `firstName` VARCHAR(191) NOT NULL DEFAULT 'Default First Name',
    ADD COLUMN `lastName` VARCHAR(191) NOT NULL DEFAULT 'Default Last Name',
    ADD COLUMN `photo` VARCHAR(191) NULL;
