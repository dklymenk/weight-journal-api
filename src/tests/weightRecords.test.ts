import bcrypt from 'bcrypt';
import request from 'supertest';
import App from '@/app';
import { CreateUserDto } from '@dtos/users.dto';
import UserRoute from '@routes/users.route';
import AuthService from '@/services/auth.service';
import WeightRecordsRoute from '@/routes/weightRecords.route';
import { CreateWeightRecordDto } from '@/dtos/weightRecords.dto';
import AuthRoute from '@/routes/auth.route';
import { User } from '@prisma/client';

afterAll(async () => {
  await new Promise<void>(resolve => setTimeout(() => resolve(), 500));
});

describe('Testing Weight Records', () => {
  const userId = 1;

  const authService = new AuthService();
  const { token } = authService.createToken(userId);

  const user: User = {
    id: 1,
    email: 'test@email.com',
    password: 'q1w2e3r4',
    firstName: 'Test Name',
    lastName: 'Test Name',
    photo: null,
    isAdmin: false,
  };

  const authRoute = new AuthRoute();
  const users = authRoute.authController.authService.users;

  users.findUnique = jest.fn().mockReturnValue({
    ...user,
    password: bcrypt.hash(user.password, 10),
  });

  describe('[POST] /weight-records', () => {
    it('response Create user', async () => {
      const dto: CreateWeightRecordDto = {
        date: '2022-04-24T15:56:41.000Z',
        weight: 50,
        note: 'test note',
      };

      const weightRecordsRoute = new WeightRecordsRoute();
      const weightRecords = weightRecordsRoute.weightRecodsController.weightRecordService.weightRecords;

      weightRecords.create = jest.fn().mockReturnValue({
        ...dto,
        id: 20,
        createdAt: '2022-04-24T15:56:41.000Z',
        userId,
      });

      const app = new App([weightRecordsRoute]);
      return request(app.getServer()).post(`${weightRecordsRoute.path}`).set('Authorization', `Bearer ${token}`).send(dto).expect(201);
    });
  });

  describe('[GET] /wieght-records', () => {
    it('response findAll weight records', async () => {
      const weightRecordsRoute = new WeightRecordsRoute();
      const weightRecords = weightRecordsRoute.weightRecodsController.weightRecordService.weightRecords;

      weightRecords.findMany = jest.fn().mockReturnValueOnce([
        {
          id: 20,
          date: '2022-04-24T15:56:41.000Z',
          createdAt: '2022-04-24T15:56:41.000Z',
          weight: 50,
          note: 'test note',
          userId,
        },
        {
          id: 21,
          date: '2022-04-24T15:56:41.000Z',
          createdAt: '2022-04-24T15:56:41.000Z',
          weight: 50,
          note: 'test note',
          userId,
        },
      ]);

      const app = new App([weightRecordsRoute]);
      return request(app.getServer()).get(`${weightRecordsRoute.path}`).set('Authorization', `Bearer ${token}`).expect(200);
    });
  });

  describe('[GET] /weight-records/:id', () => {
    it('response findOne weightRecord', async () => {
      const weightRecordId = 1;

      const weightRecordsRoute = new WeightRecordsRoute();
      const weightRecords = weightRecordsRoute.weightRecodsController.weightRecordService.weightRecords;

      weightRecords.findFirst = jest.fn().mockReturnValue({
        id: weightRecordId,
        date: '2022-04-24T15:56:41.000Z',
        createdAt: '2022-04-24T15:56:41.000Z',
        weight: 50,
        note: 'test note',
        userId,
      });

      const app = new App([weightRecordsRoute]);
      return request(app.getServer()).get(`${weightRecordsRoute.path}/${weightRecordId}`).set('Authorization', `Bearer ${token}`).expect(200);
    });
  });

  describe('[PUT] /weight-records/:id', () => {
    it('response Update wieght record', async () => {
      const weightRecordId = 1;

      const weightRecordsRoute = new WeightRecordsRoute();
      const weightRecords = weightRecordsRoute.weightRecodsController.weightRecordService.weightRecords;

      const dto: CreateWeightRecordDto = {
        date: '2022-04-24T15:56:41.000Z',
        weight: 50,
        note: 'test note',
      };

      weightRecords.findFirst = jest.fn().mockReturnValue({
        id: weightRecordId,
        date: '2022-04-24T15:56:41.000Z',
        createdAt: '2022-04-24T15:56:41.000Z',
        weight: 50,
        note: 'test note',
        userId,
      });
      weightRecords.update = jest.fn().mockReturnValue({
        id: weightRecordId,
        date: '2022-04-24T15:56:41.000Z',
        createdAt: '2022-04-24T15:56:41.000Z',
        weight: 50,
        note: 'test note',
        userId,
      });

      const app = new App([weightRecordsRoute]);
      return request(app.getServer())
        .put(`${weightRecordsRoute.path}/${weightRecordId}`)
        .set('Authorization', `Bearer ${token}`)
        .send(dto)
        .expect(200);
    });
  });

  describe('[DELETE] /weight-record/:id', () => {
    it('response Delete weight Record', async () => {
      const weightRecordId = 1;
      const dto: CreateWeightRecordDto = {
        date: '2022-04-24T15:56:41.000Z',
        weight: 50,
        note: 'test note',
      };

      const weightRecordsRoute = new WeightRecordsRoute();
      const weightRecords = weightRecordsRoute.weightRecodsController.weightRecordService.weightRecords;

      weightRecords.findFirst = jest.fn().mockReturnValue({
        ...dto,
        id: weightRecordId,
        createdAt: '2022-04-24T15:56:41.000Z',
        userId,
      });
      weightRecords.delete = jest.fn().mockReturnValue({
        ...dto,
        id: weightRecordId,
        createdAt: '2022-04-24T15:56:41.000Z',
        userId,
      });

      const app = new App([weightRecordsRoute]);
      return request(app.getServer()).delete(`${weightRecordsRoute.path}/${weightRecordId}`).set('Authorization', `Bearer ${token}`).expect(200);
    });
  });
});
