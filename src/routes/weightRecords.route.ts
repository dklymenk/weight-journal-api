import { Router } from 'express';
import { Routes } from '@interfaces/routes.interface';
import validationMiddleware from '@middlewares/validation.middleware';
import authMiddleware from '@/middlewares/auth.middleware';
import WeightRecordsController from '@/controllers/weightRecords.controller';
import { CreateWeightRecordDto } from '@/dtos/weightRecords.dto';

class WeightRecordsRoute implements Routes {
  public path = '/weight-records';
  public router = Router();
  public weightRecodsController = new WeightRecordsController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, authMiddleware, this.weightRecodsController.getWeightRecords);
    this.router.get(`${this.path}/:id(\\d+)`, authMiddleware, this.weightRecodsController.getWeightRecordById);
    this.router.post(
      `${this.path}`,
      authMiddleware,
      validationMiddleware(CreateWeightRecordDto, 'body'),
      this.weightRecodsController.createWeightRecord,
    );
    this.router.put(
      `${this.path}/:id(\\d+)`,
      authMiddleware,
      validationMiddleware(CreateWeightRecordDto, 'body', true),
      this.weightRecodsController.updateWeightRecord,
    );
    this.router.delete(`${this.path}/:id(\\d+)`, authMiddleware, this.weightRecodsController.deleteWeightRecord);
  }
}

export default WeightRecordsRoute;
