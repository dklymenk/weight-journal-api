import { NextFunction, Request, Response } from 'express';
import WeightRecordsService from '@/services/weightRecods.service';
import { RequestWithUser } from '@/interfaces/auth.interface';
import { CreateWeightRecordDto } from '@/dtos/weightRecords.dto';
import { User } from '@prisma/client';

class WeightRecordsController {
  public weightRecordService = new WeightRecordsService();

  public getWeightRecords = async (req: RequestWithUser, res: Response, next: NextFunction): Promise<void> => {
    try {
      const userData: User = req.user;
      const userId = Number(userData.id);

      const data = await this.weightRecordService.findAllWeightRecords(userId, req.query.startDate as string, req.query.endDate as string);

      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  };

  public getWeightRecordById = async (req: RequestWithUser, res: Response, next: NextFunction): Promise<void> => {
    try {
      const userData: User = req.user;
      const userId = Number(userData.id);

      const weightRecordId = Number(req.params.id);
      const data = await this.weightRecordService.findWeightRecordById(weightRecordId, userId);

      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  };

  public createWeightRecord = async (req: RequestWithUser, res: Response, next: NextFunction): Promise<void> => {
    try {
      const userData: User = req.user;
      const userId = Number(userData.id);

      const weightRecordData: CreateWeightRecordDto = req.body;
      const createWeightRecordData = await this.weightRecordService.createWeightRecord(weightRecordData, userId);

      res.status(201).json({ data: createWeightRecordData });
    } catch (error) {
      next(error);
    }
  };

  public updateWeightRecord = async (req: RequestWithUser, res: Response, next: NextFunction): Promise<void> => {
    try {
      const userData: User = req.user;
      const userId = Number(userData.id);

      const dto: CreateWeightRecordDto = req.body;

      const weightRecordId = Number(req.params.id);
      const data = await this.weightRecordService.updateWeightRecord(dto, weightRecordId, userId);

      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  };

  public deleteWeightRecord = async (req: RequestWithUser, res: Response, next: NextFunction): Promise<void> => {
    try {
      const userData: User = req.user;
      const userId = Number(userData.id);

      const weightRecordId = Number(req.params.id);
      const data = await this.weightRecordService.deleteWeightRecord(weightRecordId, userId);
      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  };
}

export default WeightRecordsController;
