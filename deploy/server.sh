# Pull code
cd sites/weight-journal-api
git checkout master
git pull origin master

# Build and deploy
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
nvm use
npm install
npm run prisma:migrate
npm run deploy:prod
